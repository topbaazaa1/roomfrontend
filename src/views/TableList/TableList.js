import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActRoom } from '../../service/Room/action';
import { ActTable } from '../../service/Table/action'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import InputLabel from "@material-ui/core/InputLabel";
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import {
  grayColor,
  defaultFont
} from "../../assets/jss/material-dashboard-react.js";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];


class Tablelist extends Component {
  constructor() {
    super();
    this.state = {
      Builds: [],
      Rooms: [],
      build: "",
      room: "",
      Table: [],
      
    };
  }
  componentDidMount() {
    let initialBuilds = [];
    fetch('http://203.150.243.108:8086/SelectBuild')
      .then(response => {
        return response.json();
      }).then(data => {
        initialBuilds = data.map((Build) => {
          return Build
        });
        console.log(initialBuilds);
        this.setState({
          Builds: initialBuilds,
        });
      });
    
}
  handleBuild = async (event) => {
    await this.setState({
      [event.target.name]: event.target.value
    });
    if (this.state.build != "") {
      this.props.ActRoom(this.state.build, (data, error) => {
        this.setState({
          Rooms: data,
        });
      });
    }
  }
  handleChange = async (event) => {
    await this.setState({
      [event.target.name]: event.target.value
    });

  console.log(this.state.Table)
  }
  hendleClick = (event) => {
    event.preventDefault()
    if (this.state.build != "" && this.state.room != "") {
      this.props.ActTable(this.state.build,this.state.room, (data, error) => {
        this.setState({
          Table: data,
        });
      });
    }
  }
  render() {
    const table = {
      minWidth: 700,
     
    }
    
    const col = {
     
      background : "#FFFAC1"
    }
    const head = {
      minWidth: 130,
      background : "#FFCD28",
      color : "#FFFFFF"
    }
    const texthead = {
      background : "#FFCD28",
      color : "#FFFFFF"
    }
    const textcol = {
      background : "#FFE65A",
      
    }

    const formControl = {
      paddingBottom: "10px",
      margin: "5px 0 0 0",
      position: "relative",
      verticalAlign: "unset",

    }
    const labelRoot = {
      ...defaultFont,
      color: grayColor[3] + " !important",
      fontWeight: "400",
      fontSize: "14px",
      lineHeight: "1.42857",
      letterSpacing: "unset"
    }
    return (
      <div>
        <React.Fragment>
          <Card>
            <CardBody>
              <GridContainer>
                <GridItem xs={6} sm={5} md={5}>
                  <FormControl fullWidth style={formControl}>
                    <InputLabel style={labelRoot}>Building</InputLabel>

                    <Select
                      native defaultValue=""
                      id="grouped-native-select"
                      name="build"
                      label="build"
                      value={this.state.build}
                      onChange={this.handleBuild}
                    >
                      <option aria-label="None" value="" />
                      {this.state.Builds.map((Build) => <option key={Build.Build}>{Build.Build}</option>)}

                    </Select>
                  </FormControl>
                </GridItem>

                <GridItem xs={6} sm={5} md={5}>
                  <FormControl fullWidth style={formControl}>
                    <InputLabel style={labelRoot}>Room</InputLabel>

                    <Select
                      native defaultValue=""
                      id="grouped-native-select"
                      name="room"
                      label="room"
                      value={this.state.room}
                      onChange={this.handleChange}
                    >
                      <option aria-label="None" value="" />
                      {this.state.Rooms.map((Room) => <option key={Room.Roomname}>{Room.Roomname}</option>)}

                    </Select>
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={2} md={2}>
                  <FormControl fullWidth style={formControl} >
                    <br />
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained" df
                      color="primary"
                      style={{ backgroundColor: '#FFA602',}}
                      onClick={this.hendleClick}
                    >
                      Search
                    </Button>
                  </FormControl>

                </GridItem>
              </GridContainer>
            </CardBody>
          </Card>
        </React.Fragment>
        <TableContainer component={Paper}>
          <Table style={table} aria-label="caption table">
           <caption>Table for showing classroom booking status (UA = Booked , A = Available ) </caption>
            <TableHead>
              <TableRow style={head} >
                <TableCell style={head} >Date(YYYY-MM-DD)</TableCell>
                <TableCell style={texthead} align="center" color = 'primary'>09.00</TableCell>
                <TableCell style={texthead} align="center">09.30</TableCell>
                <TableCell style={texthead} align="center">10.00</TableCell>
                <TableCell style={texthead} align="center">10.30</TableCell>
                <TableCell style={texthead} align="center">11.00</TableCell>
                <TableCell style={texthead} align="center">11.30</TableCell>
                <TableCell style={texthead} align="center">12.00</TableCell>
                <TableCell style={texthead} align="center">12.30</TableCell>
                <TableCell style={texthead} align="center">13.00</TableCell>
                <TableCell style={texthead} align="center">13.30</TableCell>
                <TableCell style={texthead} align="center">14.00</TableCell>
                <TableCell style={texthead} align="center">14.30</TableCell>
                <TableCell style={texthead} align="center">15.00</TableCell>
                <TableCell style={texthead} align="center">15.30</TableCell>
                <TableCell style={texthead} align="center">16.00</TableCell>
                <TableCell style={texthead} align="center">16.30</TableCell>
                <TableCell style={texthead} align="center">17.00</TableCell>
                <TableCell style={texthead} align="center">17.30</TableCell>
                <TableCell style={texthead} align="center">18.00</TableCell>
                <TableCell style={texthead} align="center">18.30</TableCell>
                <TableCell style={texthead} align="center">19.00</TableCell>
                <TableCell style={texthead} align="center">19.30</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.Table.map((table) => (
                <TableRow key={table.Dates} >
                  <TableCell style={textcol} component="th" scope="row">
                    {table.Dates}
                  </TableCell>
                  <TableCell align="center">{table.t1}</TableCell>
                  <TableCell style={col} align="center">{table.t2}</TableCell>
                  <TableCell align="center">{table.t3}</TableCell>
                  <TableCell style={col} align="center">{table.t4}</TableCell>
                  <TableCell align="center">{table.t5}</TableCell>
                  <TableCell style={col} align="center">{table.t6}</TableCell>
                  <TableCell align="center">{table.t7}</TableCell>
                  <TableCell style={col} align="center">{table.t8}</TableCell>
                  <TableCell align="center">{table.t9}</TableCell>
                  <TableCell style={col} align="center">{table.t10}</TableCell>
                  <TableCell align="center">{table.t11}</TableCell>
                  <TableCell style={col}align="center">{table.t12}</TableCell>
                  <TableCell align="center">{table.t13}</TableCell>
                  <TableCell style={col} align="center">{table.t14}</TableCell>
                  <TableCell align="center">{table.t15}</TableCell>
                  <TableCell style={col} align="center">{table.t16}</TableCell>
                  <TableCell align="center">{table.t17}</TableCell>
                  <TableCell style={col} align="center">{table.t18}</TableCell>
                  <TableCell align="center">{table.t19}</TableCell>
                  <TableCell style={col} align="center">{table.t20}</TableCell>
                  <TableCell align="center">{table.t21}</TableCell>
                  <TableCell style={col} align="center">{table.t22}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
export default connect(null, { ActTable, ActRoom })(Tablelist);
