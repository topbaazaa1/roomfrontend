import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from "../../components/Card/Card.js";

import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';


import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import InputLabel from "@material-ui/core/InputLabel";
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import CustomInput from "../../components/CustomInput/CustomInput.js";
import Button from '@material-ui/core/Button';

import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import CardFooter from "../../components/Card/CardFooter.js";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';

const useStyles = makeStyles({
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

export default function FeaturedPost(props) {
  const classes = useStyles();
  const {room } = props;  
  const [sTime, setTimestr] = React.useState('');
  const [eTime, setTimeend] = React.useState('');

  const handleChangestr = event => {
    setTimestr(event.target.value);
    console.log(sTime)

  };
  const handleChangeend = event => {
    setTimeend(event.target.value);
    console.log(eTime)
  };
  
  return (
    <Grid item xs={12} md={12}>
        <Card className={classes.card}>
          <div className={classes.cardDetails} >
            <CardBody>
              <Typography component="h2" variant="h5">
                อาคาร {room.Build}
              </Typography>
              <Typography component="h3" variant="h6">
                ห้อง : {room.Roomname}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                วันที่ : {room.Dates}
              </Typography>
              <GridContainer>
                <GridItem xs={6} sm={6} md={6}>
                  <FormControl  fullWidth className={classes.formControl}>
                    <InputLabel className={classes.labelRoot}>Start Time</InputLabel>
                    <Select
                       native defaultValue=""
                       id="grouped-native-select"
                      //  name={props.cftimestr}
                       label="timestr"
                       //value={()=> props.timestr(props.room.Time.TimeStart)}
                       value={sTime}
                       onChange={handleChangestr}
                       //onChange={this.handleChange}
                    >
                     <option aria-label="None" value="" />
                        {room.Time.map((Time) => <option key={Time.TimeStart}>{Time.TimeStart}</option>)}
                    </Select>
                  </FormControl>
                </GridItem>

                <GridItem xs={6} sm={6} md={6}>
                  <FormControl  fullWidth className={classes.formControl}>
                    <InputLabel className={classes.labelRoot}>End Time</InputLabel>
                    <Select
                      native defaultValue=""
                      id="grouped-native-select"
                     // open={open1}
                     // onClose={handleClose1}
                     // onOpen={handleOpen1}
                      //name={props.cftimeend}
                      value={eTime}
                      onChange={handleChangeend}
                    >
                     
                        {/* {room.TimeStart.map((room) => <option key={room.TimeStart}>{room.TimeStart}</option>)} */}
                        <option aria-label="None" value="" />
                        {room.Time.map((Time) => <option key={Time.TimeEnd}  
                        >{Time.TimeEnd}</option>)}
                  </Select>
                  </FormControl>
                </GridItem>
                <br/>
                <br/>
                <br/>
                {/* <GridItem xs={0} sm={0} md={1}></GridItem>
                <GridItem xs={10} sm={10} md={10}>
                 */}
                 <CardBody>
                <Button
                    variant="contained"
                    fullWidth
                    style ={{ backgroundColor: '#FFCD28',color:'#FFFFFF'}}
                    onClick={()=> props.handleClick(props.room,sTime,eTime)}
                  
                  >
                    Confirm
              </Button>
              </CardBody>
              {/* </GridItem>
              <GridItem xs={1} sm={0} md={1}></GridItem> */}
                </GridContainer>
            </CardBody>
           
                
          </div>
         
        </Card>
      
    </Grid>

  );
}

FeaturedPost.propTypes = {
  room: PropTypes.object,
  time: PropTypes.object,
};