import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActSearch } from '../../service/Search/action';
import { ActBooking } from '../../service/ฺBooking/actions'
import CssBaseline from '@material-ui/core/CssBaseline';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from "../../components/Card/Card.js";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputLabel from "@material-ui/core/InputLabel";
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import CardBody from "../../components/Card/CardBody.js";
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import {
    grayColor,
    defaultFont
} from "../../assets/jss/material-dashboard-react.js";
import FeaturedPost from './FeaturedPost';
import CustomInput from "../../components/CustomInput/CustomInput.js";
import Cookies from 'universal-cookie';
const cookies = new Cookies();
class Booking extends Component {

    constructor() {
        super();
        this.state = {
            
            activeStep: 0,
            Builds: [],
            redirectToReferrer: false,
            build: "",
            timestr: "",
            timeend: "",
            cap: "",
            date: "2020-03-16",
            message: null,
            status: "",
            rooms: [],

            fname:  cookies.get('Fname'),
            lname:  cookies.get('Lname'),
            cfbuild: "",
            cftimestr: "",
            cftimeend: "",
            cfcap: "",
            cfdate: "2020-03-16",
            cfroom: "",
            cfsubject: "",




        };
    }
    handleNext = (event) => {
        event.preventDefault();

        if (this.state.activeStep === 0) {
            if (this.state.build !== "" && this.state.timestr !== "" && this.state.status !== ""
                && this.state.timeend !== "" && this.state.cap !== "" && this.state.date !== "") {
                this.setState({ activeStep: this.state.activeStep + 1 });
                this.setState({ message: null })
                this.props.ActSearch(this.state.build, this.state.status, this.state.timestr,
                    this.state.timeend, this.state.cap, this.state.date, (data, error) => {
                        if (data) {
                    
                            this.setState({
                                rooms: data,

                            });
                            
                        }
                        else {
                            this.setState({ message: (<a>Login failed. your email or password is wrong</a>) });
                        }
                    });
            } else {

                this.setState({ message: (<a>Please complete the following information</a>) })
            }
        }
        if (this.state.activeStep === 1) {
            if (
                this.state.cfname !== "" &&
                this.state.cfbuild !== "" &&
                this.state.cftimestr !== "" &&
                this.state.cftimeend !== "" &&
                this.state.cfdate !== "" &&
                this.state.cfroom !== ""
            ) {
                this.setState({ message: null })
                this.setState({ activeStep: this.state.activeStep + 1 });
            }
            else {
                this.setState({ message: (<a>Please choose the time of room you want to book and click confirm.</a>) })
            }
        }

        if (this.state.activeStep === 2) {
            
            if (this.state.cfbuild !== "" && this.state.cftimestr !== "" && this.state.cfroom !== ""
                && this.state.cftimeend !== "" && this.state.cfname !== "" && this.state.cfdate !== "" && this.state.cfsubject !== "") {
                this.setState({ activeStep: this.state.activeStep + 1 });
                this.setState({ message: null })
                this.props.ActBooking(this.state.cfbuild, this.state.cftimestr, this.state.cfroom, this.state.cftimeend,
                    cookies.get('id'), this.state.cfdate, this.state.cfsubject, (data, error) => {
                        if (data) {
                            return <a>ฺBooking SUCESSSFUL </a>;
                        }
                        else {
                            return <a>ฺBooking UNSUCESSSFUL PLEASE TRY AGAIN</a>;
                        }
                    })
            }
            else {

                this.setState({ message: (<a>Please enter your subject</a>) })
            }
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
        this.setState({ message: null })
    };
    componentDidMount() {
        
        let initialBuilds = [];
        fetch('http://203.150.243.108:8086/SelectBuild')
            .then(response => {
                return response.json();
            }).then(data => {
                initialBuilds = data.map((Build) => {
                    return Build
                });
                
                this.setState({
                    Builds: initialBuilds,
                });
            });

    }
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    handleFieldChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }



    hendleClick = (event) => {
        event.preventDefault()
        this.setState({ rooms: [] });
        if (this.state.build !== null && this.state.timestr !== null && this.state.status !== null
            && this.state.timeend !== null && this.state.cap !== null && this.state.date !== null) {
            this.props.ActSearch(this.state.build, this.state.status, this.state.timestr,
                this.state.timeend, this.state.cap, this.state.date, (data, error) => {
                  
                    if (data) {
                       
                        this.setState({
                            rooms: data,

                        });
                    }
                    else {
                        this.setState({ message: (<a>Login failed. your email or password is wrong</a>) });
                    }
                });
        } else {
            this.setState({ message: (<a>Login failed. Please enter your email or password</a>) })
        }
    }
    getData = async  (rowData, timestart, timeend) => {
       
        await this.setState({
            cfname: "1",
            cfbuild: rowData.Build,
            cftimestr: timestart,
            cftimeend: timeend,
            cfdate: rowData.Dates,
            cfroom: rowData.Roomname
        })

       
    }
    handlehome = (event) => {
        
        event.preventDefault()
        this.setState({ activeStep: 0 });
        
    }




    render() {


        const steps = ['Search', 'Select Room', 'Booking Form'];
        const justify = {
            justifyContent: "flex-end",
            display: "flex",
            alignItems: "flex-end",
           
        }
        const justifyn = {
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            backgroundColor: '#FFA602'
        }
        const justifyb = {
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            color: '#FFA602',
            border: '1px solid #FFA602'
        }
        const justifybtn = {
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            backgroundColor: '#FFA602'
        }
        const justifys = {
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
        }
        const cardCategoryWhite = {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        }
        const cardTitleWhite = {
            color: "#FFFFFF",
            marginTop: "0px",
            minHeight: "auto",
            fontWeight: "300",
            fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
            marginBottom: "3px",
            textDecoration: "none"
        }
        const formControl = {
            paddingBottom: "10px",
            margin: "5px 0 0 0",
            position: "relative",
            verticalAlign: "unset",

        }
        const labelRoot = {
            ...defaultFont,
            color: grayColor[3] + " !important",
            fontWeight: "400",
            fontSize: "14px",
            lineHeight: "1.42857",
            letterSpacing: "unset"
        }
        const card = {
            display: 'flex',
        }
        const cardDetails = {
            flex: 1,
        }
        const textcolor = {
            coler: "red",
        }




        return (
            <div >
                <GridContainer>
                    <GridItem xs={12} sm={12} md={2} ></GridItem>
                    <GridItem xs={12} sm={12} md={8} >
                        <Card>
                            {/* <CardHeader color="primary">
                            <h4 style={cardTitleWhite} align="center">Booking Room</h4>
                        </CardHeader> */}
                            <CardBody>

                                <CssBaseline />

                                <main >

                                    <Stepper  activeStep={this.state.activeStep} >
                                        {steps.map((label) => (
                                            <Step  key={label}>
                                                <StepLabel >{label}</StepLabel>
                                            </Step>
                                        ))}
                                    </Stepper>
                                    <React.Fragment>
                                        {this.state.activeStep === steps.length ? (
                                            <React.Fragment >
                                                <Typography variant="h5" gutterBottom style={justifys}>
                                                    Thank you for your booking.
                                    </Typography>
                                                <Typography variant="body1" gutterBottom style={justifys}>
                                                    Your booking is sucess.
                                    </Typography>
                                                <div style={justifys}>
                                                    <br /><br /><br />
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        onClick={this.handlehome}
                                                        style={justifybtn}


                                                    >
                                                        home
                                            </Button>
                                                </div>
                                            </React.Fragment>
                                        ) : (
                                                <React.Fragment>
                                                    {/* {getStepContent(activeStep)} */}
                                                    {this.state.activeStep === 0 && (
                                                        <React.Fragment>
                                                            <div>
                                                                <GridContainer>
                                                                    <CardBody>
                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <FormControl fullWidth style={formControl}>
                                                                                    <InputLabel style={labelRoot}>Building</InputLabel>

                                                                                    <Select
                                                                                        native defaultValue=""
                                                                                        id="grouped-native-select"
                                                                                        name="build"
                                                                                        label="build"
                                                                                        value={this.state.build}
                                                                                        onChange={this.handleChange}
                                                                                    >
                                                                                        <option aria-label="None" value="" />
                                                                                        {this.state.Builds.map((Build) => <option key={Build.Build}>{Build.Build}</option>)}

                                                                                    </Select>
                                                                                </FormControl>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        id="date"
                                                                                        label="date"
                                                                                        type="date"
                                                                                        name="date"
                                                                                        fullWidth
                                                                                        autoFocus
                                                                                        //defaultValue="2019-02-25"
                                                                                        value={this.state.date}
                                                                                        onChange={this.handleFieldChange}
                                                                                    //InputLabelProps={{
                                                                                    //  shrink: true,
                                                                                    //}}
                                                                                    />

                                                                                </form>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={6}>
                                                                                <FormControl fullWidth style={formControl}>
                                                                                    <InputLabel id="demo-simple-select-outlined-label">
                                                                                        Start Time
                                                                        </InputLabel>
                                                                                    <Select
                                                                                        native defaultValue=""
                                                                                        id="grouped-native-select"
                                                                                        name="timestr"
                                                                                        label="timestr"
                                                                                        value={this.state.timestr}
                                                                                        onChange={this.handleChange}
                                                                                    //value={age1}
                                                                                    //onChange={handleChange1}
                                                                                    //labelWidth={labelWidth}
                                                                                    >
                                                                                        <option aria-label="None" value="" />
                                                                                        <option value="9:00:00">9:00:00</option>
                                                                                        <option value="9:30:00">9:30:00</option>
                                                                                        <option value="10:00:00">10:00:00</option>
                                                                                        <option value="10:30:00">10:30:00</option>
                                                                                        <option value="11:00:00">11:00:00</option>
                                                                                        <option value="11:30:00">11:30:00</option>
                                                                                        <option value="12:00:00">12:00:00</option>
                                                                                        <option value="12:30:00">12:30:00</option>
                                                                                        <option value="13:00:00">13:00:00</option>
                                                                                        <option value="13:30:00">13:30:00</option>
                                                                                        <option value="14:00:00">14:00:00</option>
                                                                                        <option value="14:30:00">14:30:00</option>
                                                                                        <option value="15:00:00">15:00:00</option>
                                                                                        <option value="15:30:00">15:30:00</option>
                                                                                        <option value="16:00:00">16:00:00</option>
                                                                                        <option value="16:30:00">16:30:00</option>
                                                                                        <option value="17:00:00">17:00:00</option>
                                                                                        <option value="17:30:00">17:30:00</option>
                                                                                        <option value="18:00:00">18:00:00</option>
                                                                                        <option value="18:30:00">18:30:00</option>
                                                                                        <option value="19:00:00">19:00:00</option>
                                                                                        <option value="19:30:00">19:30:00</option>
                                                                                    </Select>
                                                                                </FormControl></GridItem>

                                                                            <GridItem xs={12} sm={12} md={6}>
                                                                                <FormControl fullWidth style={formControl}>
                                                                                    <InputLabel id="demo-simple-select-outlined-label">
                                                                                        End Time
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        native defaultValue=""
                                                                                        id="grouped-native-select"
                                                                                        name="timeend"
                                                                                        label="timeend"
                                                                                        value={this.state.timeend}
                                                                                        onChange={this.handleChange}
                                                                                    //value={age2}
                                                                                    //onChange={handleChange2}
                                                                                    //labelWidth={labelWidth}
                                                                                    >
                                                                                        <option aria-label="None" value="" />
                                                                                        <option value="9:30:00">9:30:00</option>
                                                                                        <option value="10:00:00">10:00:00</option>
                                                                                        <option value="10:30:00">10:30:00</option>
                                                                                        <option value="11:00:00">11:00:00</option>
                                                                                        <option value="11:30:00">11:30:00</option>
                                                                                        <option value="12:00:00">12:00:00</option>
                                                                                        <option value="12:00:00">12:30:00</option>
                                                                                        <option value="13:00:00">13:00:00</option>
                                                                                        <option value="13:30:00">13:30:00</option>
                                                                                        <option value="14:00:00">14:00:00</option>
                                                                                        <option value="14:30:00">14:30:00</option>
                                                                                        <option value="15:00:00">15:00:00</option>
                                                                                        <option value="15:30:00">15:30:00</option>
                                                                                        <option value="16:00:00">16:00:00</option>
                                                                                        <option value="16:30:00">16:30:00</option>
                                                                                        <option value="17:00:00">17:00:00</option>
                                                                                        <option value="17:30:00">17:30:00</option>
                                                                                        <option value="18:00:00">18:00:00</option>
                                                                                        <option value="18:30:00">18:30:00</option>
                                                                                        <option value="19:00:00">19:00:00</option>
                                                                                        <option value="19:30:00">19:30:00</option>
                                                                                        <option value="20:00:00">20:00:00</option>
                                                                                    </Select>
                                                                                </FormControl></GridItem>

                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <FormControl fullWidth style={formControl}>
                                                                                    <InputLabel id="demo-simple-select-outlined-label">
                                                                                        Status
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        native defaultValue=""
                                                                                        id="grouped-native-select"
                                                                                        name="status"
                                                                                        label="status"
                                                                                        value={this.state.status}
                                                                                        onChange={this.handleChange}
                                                                                    //value={age3}
                                                                                    //onChange={handleChange3}
                                                                                    //labelWidth={labelWidth}
                                                                                    >
                                                                                        <option aria-label="None" value="" />
                                                                                        <option value="A">ว่าง</option>
                                                                                        {/* <option value="W">รอดำเนินการ</option> */}
                                                                                        {/* <option value="UA">จองแล้ว</option> */}
                                                                                    </Select>
                                                                                </FormControl></GridItem>

                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <FormControl fullWidth style={formControl}>
                                                                                    <InputLabel id="demo-simple-select-outlined-label">
                                                                                        Cappacity
                                                                                    </InputLabel>
                                                                                    <Select
                                                                                        native defaultValue=""
                                                                                        id="grouped-native-select"
                                                                                        name="cap"
                                                                                        label="cap"
                                                                                        value={this.state.cap}
                                                                                        onChange={this.handleChange}
                                                                                    //value={age3}
                                                                                    //onChange={handleChange3}
                                                                                    //labelWidth={labelWidth}
                                                                                    >
                                                                                        <option aria-label="None" value="" />
                                                                                        <option value={60}>60</option>

                                                                                    </Select>
                                                                                </FormControl>
                                                                            </GridItem></GridContainer>
                                                                        <br />
                                                                        <p style={{ color: "red" }}>{this.state.message}</p>

                                                                    </CardBody>
                                                                </GridContainer>

                                                            </div>

                                                        </React.Fragment>)}
                                                    {this.state.activeStep === 1 && (
                                                        <React.Fragment>
                                                            <GridContainer>
                                                                <CardBody>
                                                                    <GridContainer>
                                                                        <GridItem xs={12} sm={12} md={4}>
                                                                            <FormControl fullWidth style={formControl}>
                                                                                <InputLabel style={labelRoot}>Building</InputLabel>

                                                                                <Select
                                                                                    native defaultValue=""
                                                                                    id="grouped-native-select"
                                                                                    name="build"
                                                                                    label="build"
                                                                                    value={this.state.build}
                                                                                    onChange={this.handleChange}
                                                                                >
                                                                                    <option aria-label="None" value="" />
                                                                                    {this.state.Builds.map((Build) => <option key={Build.Build}>{Build.Build}</option>)}

                                                                                </Select>
                                                                            </FormControl>
                                                                        </GridItem>

                                                                        <GridItem xs={12} sm={12} md={4}>
                                                                            <form style={formControl} noValidate>
                                                                                <TextField
                                                                                    id="date"
                                                                                    label="date"
                                                                                    type="date"
                                                                                    name="date"
                                                                                    fullWidth
                                                                                    autoFocus
                                                                                    //defaultValue="2019-02-25"
                                                                                    value={this.state.date}
                                                                                    onChange={this.handleFieldChange}
                                                                                //InputLabelProps={{
                                                                                //  shrink: true,
                                                                                //}}
                                                                                />

                                                                            </form>
                                                                        </GridItem>

                                                                        <GridItem xs={12} sm={12} md={2}>
                                                                            <FormControl fullWidth style={formControl}>
                                                                                <InputLabel id="demo-simple-select-outlined-label">
                                                                                    Start Time
                                                                        </InputLabel>
                                                                                <Select
                                                                                    native defaultValue=""
                                                                                    id="grouped-native-select"
                                                                                    name="timestr"
                                                                                    label="timestr"
                                                                                    value={this.state.timestr}
                                                                                    onChange={this.handleChange}
                                                                                //value={age1}
                                                                                //onChange={handleChange1}
                                                                                //labelWidth={labelWidth}
                                                                                >
                                                                                    <option aria-label="None" value="" />
                                                                                    <option value="9:00:00">9:00:00</option>
                                                                                    <option value="9:30:00">9:30:00</option>
                                                                                    <option value="10:00:00">10:00:00</option>
                                                                                    <option value="10:30:00">10:30:00</option>
                                                                                    <option value="11:00:00">11:00:00</option>
                                                                                    <option value="11:30:00">11:30:00</option>
                                                                                    <option value="12:00:00">12:00:00</option>
                                                                                    <option value="12:30:00">12:30:00</option>
                                                                                    <option value="13:00:00">13:00:00</option>
                                                                                    <option value="13:30:00">13:30:00</option>
                                                                                    <option value="14:00:00">14:00:00</option>
                                                                                    <option value="14:30:00">14:30:00</option>
                                                                                    <option value="15:00:00">15:00:00</option>
                                                                                    <option value="15:30:00">15:30:00</option>
                                                                                    <option value="16:00:00">16:00:00</option>
                                                                                    <option value="16:30:00">16:30:00</option>
                                                                                    <option value="17:00:00">17:00:00</option>
                                                                                    <option value="17:30:00">17:30:00</option>
                                                                                    <option value="18:00:00">18:00:00</option>
                                                                                    <option value="18:30:00">18:30:00</option>
                                                                                    <option value="19:00:00">19:00:00</option>
                                                                                    <option value="19:30:00">19:30:00</option>
                                                                                </Select>
                                                                            </FormControl></GridItem>

                                                                        <GridItem xs={12} sm={12} md={2}>
                                                                            <FormControl fullWidth style={formControl}>
                                                                                <InputLabel id="demo-simple-select-outlined-label">
                                                                                    End Time
                                                                        </InputLabel>
                                                                                <Select
                                                                                    native defaultValue=""
                                                                                    id="grouped-native-select"
                                                                                    name="timeend"
                                                                                    label="timeend"
                                                                                    value={this.state.timeend}
                                                                                    onChange={this.handleChange}
                                                                                //value={age2}
                                                                                //onChange={handleChange2}
                                                                                //labelWidth={labelWidth}
                                                                                >
                                                                                    <option aria-label="None" value="" />
                                                                                    <option value="9:30:00">9:30:00</option>
                                                                                    <option value="10:00:00">10:00:00</option>
                                                                                    <option value="10:30:00">10:30:00</option>
                                                                                    <option value="11:00:00">11:00:00</option>
                                                                                    <option value="11:30:00">11:30:00</option>
                                                                                    <option value="12:00:00">12:00:00</option>
                                                                                    <option value="12:00:00">12:30:00</option>
                                                                                    <option value="13:00:00">13:00:00</option>
                                                                                    <option value="13:30:00">13:30:00</option>
                                                                                    <option value="14:00:00">14:00:00</option>
                                                                                    <option value="14:30:00">14:30:00</option>
                                                                                    <option value="15:00:00">15:00:00</option>
                                                                                    <option value="15:30:00">15:30:00</option>
                                                                                    <option value="16:00:00">16:00:00</option>
                                                                                    <option value="16:30:00">16:30:00</option>
                                                                                    <option value="17:00:00">17:00:00</option>
                                                                                    <option value="17:30:00">17:30:00</option>
                                                                                    <option value="18:00:00">18:00:00</option>
                                                                                    <option value="18:30:00">18:30:00</option>
                                                                                    <option value="19:00:00">19:00:00</option>
                                                                                    <option value="19:30:00">19:30:00</option>
                                                                                    <option value="20:00:00">20:00:00</option>
                                                                                </Select>
                                                                            </FormControl></GridItem>
                                                                    </GridContainer>
                                                                    <GridContainer>
                                                                        <GridItem xs={12} sm={12} md={4}>
                                                                            <FormControl fullWidth style={formControl}>
                                                                                <InputLabel id="demo-simple-select-outlined-label">
                                                                                    Status
                                                                        </InputLabel>
                                                                                <Select
                                                                                    native defaultValue=""
                                                                                    id="grouped-native-select"
                                                                                    name="status"
                                                                                    label="status"
                                                                                    value={this.state.status}
                                                                                    onChange={this.handleChange}
                                                                                //value={age3}
                                                                                //onChange={handleChange3}
                                                                                //labelWidth={labelWidth}
                                                                                >
                                                                                    <option aria-label="None" value="" />
                                                                                    <option value="A">ว่าง</option>
                                                                                    {/* <option value="W">รอดำเนินการ</option>
                                                                                    <option value="UA">จองแล้ว</option> */}
                                                                                </Select>
                                                                            </FormControl></GridItem>

                                                                        <GridItem xs={12} sm={12} md={4}>
                                                                            <FormControl fullWidth style={formControl}>
                                                                                <InputLabel id="demo-simple-select-outlined-label">
                                                                                    Cappacity
                                                                        </InputLabel>
                                                                                <Select
                                                                                    native defaultValue=""
                                                                                    id="grouped-native-select"
                                                                                    name="cap"
                                                                                    label="cap"
                                                                                    value={this.state.cap}
                                                                                    onChange={this.handleChange}
                                                                                //value={age3}
                                                                                //onChange={handleChange3}
                                                                                //labelWidth={labelWidth}
                                                                                >
                                                                                    <option aria-label="None" value="" />
                                                                                    <option value={60}>60</option>

                                                                                </Select>
                                                                            </FormControl></GridItem>

                                                                        <GridItem xs={12} sm={12} md={4}>
                                                                            <FormControl fullWidth style={formControl} >
                                                                                <br />
                                                                                <Button
                                                                                    type="submit"
                                                                                    fullWidth
                                                                                    variant="contained" df
                                                                                    style={{ backgroundColor: '#FFA602',color:'#FFFFFF'}}
                                                                                    onClick={this.hendleClick}
                                                                                >
                                                                                    Search
                                                                        </Button>
                                                                            </FormControl>

                                                                        </GridItem></GridContainer>

                                                                    <br />

                                                                </CardBody>
                                                            </GridContainer>
                                                            <main>
                                                                <Grid container spacing={4}>
                                                                    {this.state.rooms.map(room => (
                                                                        <FeaturedPost key={room.Roomname} room={room} handleClick={this.getData} />
                                                                    ))}
                                                                </Grid>



                                                            </main>
                                                            <p style={{
                                                                color: "red", 
                                                                justifyContent: "flex-end",
                                                                display: "flex",
                                                                alignItems: "flex-end",
                                                            }}>{this.state.message}</p>
                                                        </React.Fragment>)}
                                                    {this.state.activeStep === 2 && (
                                                        <React.Fragment>
                                                          <GridContainer>
                                                                <GridItem xs={12} sm={12} md={12}>
                                                                    <CardBody>
                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={6}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField labelText={this.state.fname}
                                                                                        label="FirstName"
                                                                                        id="firstName"
                                                                                        defaultValue={this.state.fname}
                                                                                        fullWidth
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                            <GridItem xs={12} sm={12} md={6} >
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        labelText={this.state.lname}
                                                                                        defaultValue={this.state.lname}
                                                                                        label="LastName"
                                                                                        id="lastName"

                                                                                        fullWidth
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <form style={formControl} noValidate>

                                                                                    <TextField
                                                                                        id="cfsubject"
                                                                                        type="cfsubject"


                                                                                        label="ฺSubject"
                                                                                        name="cfsubject"
                                                                                        value={this.state.cfsubject}
                                                                                        onChange={this.handleFieldChange}

                                                                                        fullWidth


                                                                                    />

                                                                                </form>

                                                                            </GridItem>
                                                                        </GridContainer>
                                                                        <p style={{ color: "red" }}>{this.state.message}</p>
                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={9}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        labelText={this.state.cfbuild}
                                                                                        defaultValue={this.state.cfbuild}
                                                                                        id="building"
                                                                                        label="Build"

                                                                                        fullWidth

                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'

                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                            <GridItem xs={12} sm={12} md={3} >
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        labelText={this.state.cfroom}
                                                                                        defaultValue={this.state.cfroom}
                                                                                        id="room"
                                                                                        label="Room"
                                                                                        fullWidth

                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        id="date"
                                                                                        label="Date"
                                                                                        type="date"
                                                                                        defaultValue={this.state.cfdate}
                                                                                        fullWidth
                                                                                        disabled='flase'
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={6}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        id="sTime"
                                                                                        label="Start Time"
                                                                                        type="time"
                                                                                        defaultValue={this.state.cftimestr}
                                                                                        fullWidth
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'
                                                                                        inputProps={{
                                                                                            step: 1800, // 30 min
                                                                                        }}
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                            <GridItem xs={12} sm={12} md={6}>
                                                                                <form style={formControl} noValidate>
                                                                                    <TextField
                                                                                        id="eTime"
                                                                                        label="End Time"
                                                                                        type="time"
                                                                                        defaultValue={this.state.cftimeend}
                                                                                        fullWidth
                                                                                        InputLabelProps={{
                                                                                            shrink: true,
                                                                                        }}
                                                                                        disabled='flase'
                                                                                        inputProps={{
                                                                                            step: 1800, // 30 min
                                                                                        }}
                                                                                    />
                                                                                </form>
                                                                            </GridItem>
                                                                        </GridContainer>

                                                                        <GridContainer>
                                                                            <GridItem xs={12} sm={12} md={12}>
                                                                                <InputLabel style={{ color: "#AAAAAA" }}>Description</InputLabel>
                                                                                <CustomInput
                                                                                    labelText=""
                                                                                    id="desc"
                                                                                    formControlProps={{
                                                                                        fullWidth: true
                                                                                    }}
                                                                                    inputProps={{
                                                                                        multiline: true,
                                                                                        rows: 5
                                                                                    }}
                                                                                />
                                                                            </GridItem>
                                                                        </GridContainer>
                                                                    </CardBody>
                                                                </GridItem>
                                                            </GridContainer>
                                                        </React.Fragment>
                                                    )}
                                                    <div style={justify}>
                                                        <br />
                                                        <br />
                                                        <br />
                                                        {this.state.activeStep !== 0 && (
                                                            <Button onClick={this.handleBack}
                                                                style={justifyb}
                                                                variant="outlined"
                                                                color="#FFA602">
                                                                Back
                                                            </Button>

                                                        )}
                                             &nbsp;
                                            <Button
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={this.handleNext}
                                                            style={justifyn}

                                                        >
                                                            {this.state.activeStep === steps.length - 1 ? 'Book' : 'Next'}
                                                        </Button>
                                                    </div>
                                                </React.Fragment>
                                            )}
                                    </React.Fragment>


                                </main>
                            </CardBody>
                        </Card>
                    </GridItem>
                    <GridItem xs={12} sm={12} md={2} ></GridItem>
                </GridContainer>
            </div>
        );
    }
}
export default connect(null, { ActSearch, ActBooking })(Booking);