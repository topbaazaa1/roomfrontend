import axios from 'axios';
import { useCallback } from 'react';
import {DELETE} from './type'; 

export const ActDelete = (bookingId, callback = null) => {
    return dispatch =>{
        
        const API_PATH = 'http://203.150.243.108:8086/delete';
        axios(API_PATH, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            data:{
                BookingId: bookingId,
                
    
            }
            
        })
    
    .then(reponse => {
        if(reponse.data){
            dispatch({type:DELETE, payload: reponse.data});
           
        }
        if(callback != null){
            callback(reponse.data,null);
        }
    })
        .catch(error => {
            console.log(error);
            callback(null,error);
    
        })
    }
}