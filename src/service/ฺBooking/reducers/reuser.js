import {BOOKING} from '..//actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case BOOKING:
            return action.payload;
        default:
            return state;
    }
}