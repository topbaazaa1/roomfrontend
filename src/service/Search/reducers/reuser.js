import {SEARCH} from '..//actions/type';
export const UserReducer = (state = [], action) => {
    switch(action.type){
        case SEARCH:
            return action.payload;
        default:
            return state;
    }
}