/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons

import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import TableChart from "@material-ui/icons/TableChart";

// core components/views for Admin layout

import Main from "./views/Booking/Main";
import TableList from "./views/TableList/TableList.js";
import MyBooking from "./views/MyBooking/myBooking.js";



// core components/views for RTL layout


const dashboardRoutes = [
  {
    path: "/mybooking",
    name: "My Booking",
   
    icon: Person,
    component: MyBooking,
    layout: "/user"
  },
  {
    path: "/booking",
    name: "Booking",
    
    icon: LibraryBooks,
    component: Main,
    layout: "/user"
  },
  {
    path: "/table",
    name: "Table List",
    
    icon: TableChart,
    component: TableList,
    layout: "/user"
  },
  
  
];

export default dashboardRoutes;
